import pandas as pd
import pytest

import src.test_input_dataset as ti


@pytest.fixture
def basic_tweet():
    return "test de tweet basique et simple et court"


@pytest.fixture
def tweet_rep():
    return "test de tweet test de tweet"

@pytest.fixture()
def tweet_short():
    return "tweet"


def test_init_class_should_have_correct_attributes_values():
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "bldfgldfg"
    with pytest.raises(ValueError):
        ti.InputDataset(input_file_path, dataset_type)


def test_init_class_should_raise_file_not_found_exception():
    input_file_path = "data/fdsfdsfdtest_ds.csv"
    dataset_type = "learn"
    with pytest.raises(FileNotFoundError):
        ti.InputDataset(input_file_path, dataset_type)


def test_init_class_should_load_dataset():
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    assert (type(inst._dataset) == pd.DataFrame)


def test_split_train_test_split_should_split_correctly():
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    inst.train_valid_split(0.8)
    assert (type(inst._train_split) == pd.DataFrame)
    assert (type(inst._valid_split) == pd.DataFrame)
    assert (len(inst._train_split) == 6090)
    assert (len(inst._valid_split) == 1523)


def test_extract_word_ngrams_1gram_should_return_correct_unique_words(basic_tweet):
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    res = inst.extract_word_ngrams(basic_tweet, size_ngram=1)
    assert (res == {"test": 1, "tweet": 1, "basique": 1, "simple": 1, "court": 1, "et": 2, "de": 1})


def test_extract_word_ngrams_2gram_should_return_correct_unique_counts(basic_tweet):
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    res = inst.extract_word_ngrams(basic_tweet, size_ngram=2)
    assert (res == {"test de": 1, "de tweet": 1, "tweet basique": 1, "basique et": 1, "et simple": 1, "simple et": 1,
                    "et court": 1})


def test_extract_word_ngrams_3gram_should_return_correct_unique_counts(basic_tweet):
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    res = inst.extract_word_ngrams(basic_tweet, size_ngram=3)
    assert (res == {"test de tweet": 1, "de tweet basique": 1, "tweet basique et": 1, "basique et simple": 1,
                    "et simple et": 1, "simple et court": 1})


def test_extract_word_ngrams_3gram_should_return_correct_multiple_counts(tweet_rep):
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    res = inst.extract_word_ngrams(tweet_rep, size_ngram=3)
    assert (res == {"test de tweet": 2, "de tweet test": 1, "tweet test de": 1})


def test_extract_word_ngrams_longer_than_tweet_should_return_empty_dict(tweet_short):
    input_file_path = "../data/test_train_ds.csv"
    dataset_type = "learn"
    inst = ti.InputDataset(input_file_path, dataset_type)
    res = inst.extract_word_ngrams(tweet_short, size_ngram=3)
    assert (res == {})
