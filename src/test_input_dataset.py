import pandas as pd
import logging as log


class InputDataset:

    def __init__(self, input_file_path, dataset_type):
        self._input_file_path = input_file_path
        if dataset_type in {"learn", "apply"}:
            self._dataset_type = dataset_type
        else:
            raise ValueError("Define a correct dataset_type : learn / apply")

        self._dataset = pd.read_csv(self._input_file_path)
        log.info(self._dataset_type + " was correctly loaded")

    def train_valid_split(self, train_pct):
        self._train_split = self._dataset.sample(frac=train_pct)
        self._valid_split = self._dataset.sample(frac=1 - train_pct)

    def extract_word_ngrams(self, basic_tweet, size_ngram=1):
        arr = basic_tweet.split()
        ret_dict = {}
        len_arr = len(arr)
        for i in range(0, len_arr):
            if (i + size_ngram) <= (len_arr):
                ngram = arr[i:i + size_ngram]
                ngram = ' '.join(ngram)
                if ngram in ret_dict.keys():
                    ret_dict[ngram] = ret_dict[ngram] + 1
                else:
                    ret_dict[ngram] = 1

        return ret_dict
